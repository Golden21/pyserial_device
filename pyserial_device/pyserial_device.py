import os
import time
import pathlib
from queue import Queue
from threading import Event, RLock, Thread

import serial
from protocol_raw import protocol_raw


class pyserial_device(object):
    """
    class used to provide easy to use features for serial communication.
    """

    def __del__(self):
        self.disable_async_read()
        self.disable_log()
        self.close()

    def __init__(self):
        """
        Initialize the serial port.
        The port will not be opened ! use open method for this purpose.
        """
        self.__lock__ = RLock()
        self.__serial_port__ = serial.Serial()
        self.__protocol__ = None
        self.__reading_thread__ = None
        self.__received_frame_callback__ = None
        self.__async_enabled__ = Event()  # evenement pour arreter le thread de lecture continue
        self.__async_enabled__.clear()
        self.__input_bytestream__ = bytearray()
        self.__query_stub_input__ = []
        self.__stub__ = False
        self.__last_written_frame__ = bytearray()
        self.__stub_write_frames_queue__ = Queue()
        self.__port_config__ = {"port": None, "baudrate": None, "parity": None, "stopbits": None, "timeout": None}
        self.__log_file__ = None
        self.__log_callback_rx__ = None
        self.__log_callback_tx__ = None
        self.set_protocol(protocol_raw)

    @property
    def protocol(self):
        return self.__protocol__

    @property
    def last_written_frame(self):
        return self.__last_written_frame__.hex().upper()

    @property
    def log_enabled(self):
        return not (self.current_log_file is None)

    @property
    def stub_enabled(self):
        return self.__stub__

    @property
    def current_port_config(self):
        return self.__port_config__

    @property
    def is_opened(self):
        if self.__stub__:
            return True
        else:
            return self.__serial_port__.isOpen()

    def open(self, port, baudrate, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout=None):
        """
        Open the serial port. Returns True if success, False otherwise.
        arguments:
        -- port: the port name (ex: "COM16"). The special string "STUB" can be used to automatically enable stub mode before opening.
        -- baudrate: the baudrate of the communication (int)
        -- parity: the parity. Need to import serial and use serial.PARITY_NONE, serial.PARITY_ODD or serial.PARITY_EVEN
        -- stopbits: stopbits. Need to import serial and use serial.STOPBITS_ONE or serial.STOPBITS_TWO
        -- timeout: ms
        """
        try:
            if self.is_opened:
                self.close()
            if port.startswith("COM"):
                port = "\\\\.\\" + port
            self.__port_config__ = {
                "port": port,
                "baudrate": baudrate,
                "parity": parity,
                "stopbits": stopbits,
                "timeout": timeout,
            }
            if port == "STUB" or self.stub_enabled:
                self.enable_stub()
            else:
                self.disable_stub()
            if self.__stub__:
                return True
            elif not self.is_opened:
                self.__serial_port__.port = port
                self.__serial_port__.baudrate = baudrate
                self.__serial_port__.parity = parity
                self.__serial_port__.stopbits = stopbits
                self.__serial_port__.timeout = timeout  # attente infinie (read bloquant) si None
                self.__serial_port__.open()
                return self.is_opened
            else:
                raise Exception(f"port {self.__serial_port__.name} already opened.")
        except Exception as e:
            raise Exception(str(e))

    def close(self):
        """
        Close the serial port. Returns True if success, False otherwise.
        """
        if self.is_opened:
            if self.__stub__:
                self.disable_async_read()
                self.clear()
                return True
            else:
                self.disable_async_read()
                self.__serial_port__.close()
                self.clear()
                return not self.is_opened

    def read(self, format_bytes_to_hexstr=False):
        """
        Flush frames from the self._frames_queue, and returns them as a list.
        apply self.__received_frame_callback__ before returning, if it is set.
        frames are composed of a tuple (bool, bytearray) (default) or (bool, hexa_str)
        -- format_bytes_to_hexstr: boolean. if True, bytearrays will be converted to hexa str (usefull for pyro calls)
        """
        answers = []
        if (
            not self.stub_enabled and self.__reading_thread__ is None
        ):  # lecture uniquement si le thread ne tourne pas et qu'on est pas en mode stub
            try:
                with self.__lock__:
                    self.__input_bytestream__ += self.__serial_port__.read(self.__serial_port__.in_waiting)
            except Exception as e:
                raise Exception(str(e))
        with self.__lock__:
            self.__protocol__.process_bytestream(self.__input_bytestream__)
            answers = self.__protocol__.get_frames()
            for status, frame in answers:
                if self.current_log_file is not None:
                    self.__log_callback_rx__(self.current_log_file, status, frame)
                if self.__received_frame_callback__ is not None:
                    self.__received_frame_callback__(status, frame)
        if format_bytes_to_hexstr is False:
            return answers
        else:
            return [(status, frame.hex().upper()) for (status, frame) in answers]

    def write(self, bytestream):
        """
        Write bytes to the serial port.
        Parameters:
        -- bytestream: a bytearray or hexa_str
        """
        if type(bytestream) is str:
            if len(bytestream) % 2 != 0:
                bytestream = f"0{bytestream}"
            bytestream = bytearray.fromhex(bytestream)
        if self.stub_enabled:
            with self.__lock__:
                if self.log_enabled:
                    self.__log_callback_tx__(self.current_log_file, True, bytestream)
            self.__stub_write_frames_queue__.put(bytestream)
            self.__last_written_frame__ = bytearray(bytestream)
            return len(bytestream)
        else:
            try:
                with self.__lock__:
                    nb_written = self.__serial_port__.write(bytestream)
                    if self.log_enabled:
                        self.__log_callback_tx__(self.current_log_file, nb_written == len(bytestream), bytestream)
                    self.__last_written_frame__ = bytearray(bytestream)
                    return nb_written
            except Exception as e:
                if self.log_enabled:
                    self.__log_callback_tx__(self.current_log_file, False, bytestream)
                raise Exception(str(e))

    def query(self, bytestream, timeout_s=1, request_checking_callback=None, format_bytes_to_hexstr=False):
        """
        Query bytes on the serial port (write and read the answers).
        Is used to wait until a specific frame is received or timeout is over.
        return at first condition met: timeout expired, or request_checking_callback is True for at least 1 received frame.
        Parameters:
        -- bytestream: a bytearray or hexa_str
        -- request_checking_callback: optionnal. a function with prototype f(status(bool), frame(bytearray)), wich returns a boolean indicating the frame is the expected one.
        -- format_bytes_to_hexstr: boolean. if True, bytearrays will be converted to hexa str (usefull for pyro calls)
        returns: tuple (bytearray, [(bool, bytearray), (...)]): the frame which matched the request, and all the frames received during function call.
        """
        if self.__stub__:
            timeout_s = 0
        restore = self.__reading_thread__ is not None
        self.disable_async_read()
        self.read()  # flush the buffer before query.
        if request_checking_callback is None:
            request_checking_callback = lambda status, frame: True
        if self.__stub__:
            with self.__lock__:
                if len(self.__query_stub_input__) > 0:
                    self.__input_bytestream__ += self.__query_stub_input__.pop(0)
        self.write(bytestream)
        all_answers = []
        query_answer = bytearray()
        query_satisfied = False
        start = time.time()
        timed_out = False
        while not timed_out and not query_satisfied:
            if timeout_s:
                # When a timeout occur, notify the protocol (it might decide to discard some data) then run one last read and exit the loop
                timed_out = time.time() - start > timeout_s
                if timed_out:
                    self.__protocol__.on_timeout()
            # If timeout_s is zero, just end the loop after one call without triggering on_timeout()
            else:
                timed_out = True
            tmp = self.read()
            all_answers += tmp
            for status, frame in tmp:
                query_satisfied = request_checking_callback(status, frame)
                if query_satisfied:
                    query_answer = frame
                    break
        if restore:
            self.enable_async_read()
        if format_bytes_to_hexstr is False:
            return query_answer, all_answers
        else:
            return (query_answer.hex().upper(), [(status, frame.hex().upper()) for (status, frame) in all_answers])

    def set_protocol(self, protocol_cls):
        """
        Set a protocol used to parse bytes read from the serial port.
        Arguments:
        -- protocol_cls: either a class (not instance !) of a protocol: for local calls. Or a serialized protocol, obtained by calling protocol_serial_fpga on the class: for remote calls.
        Exemple:
        - local call: set_protocol(protocol_serial_fpga)
        - remote call (through Pyro4): set_protocol(pyro4_pickling(protocol_serial_fpga))
        """
        with self.__lock__:
            instanciated_protocol = protocol_cls()
            assert isinstance(
                instanciated_protocol, protocol_raw
            ), f"error instaciating protocol of class {protocol_cls}."
            self.__protocol__ = instanciated_protocol
            self.__log_callback_rx__ = self.protocol.default_log_rx
            self.__log_callback_tx__ = self.protocol.default_log_tx

    def enable_log(self, log_path):
        if self.__log_file__ is None:
            self.__log_file__ = open(log_path, "w")

    def disable_log(self):
        if self.__log_file__ is not None:
            self.__log_file__.close()
            file_path = pathlib.Path(self.__log_file__.name)
            empty = os.stat(file_path).st_size == 0
            if empty:
                os.remove(file_path)
            self.__log_file__ = None

    def set_received_frame_callback(self, received_frame_callback):
        """
        Set a callback to be called for each frame sucessfully parsed by the class (read_routine or read method).
        Parameters:
        received_frame_callback: the callback. protoype must be: callback(status, frame), where frame is a bytearray and status is a boolean.
        """
        self.__received_frame_callback__ = received_frame_callback

    def enable_async_read(self, rx_callback=None):
        """
        Enable continuous read.
        parameters:
        -- rx_callback: a function with prototype f(status, frame), to call when a frame is received.
        status: bool
        frame: bytearray
        """
        if self.__reading_thread__ is None:
            self.__reading_thread__ = Thread(target=self.__read_routine__, daemon=True)
            if rx_callback:
                self.set_received_frame_callback(rx_callback)
            self.__async_enabled__.set()
            self.__reading_thread__.start()

    def disable_async_read(self):
        """
        Disable continuous read.
        """
        self.__async_enabled__.clear()
        if self.__reading_thread__ is not None and self.is_opened:
            if not self.__stub__:
                while self.__reading_thread__.is_alive():
                    # cancel_read n'annule que si ya une lecture en cours au moment de l'appel.
                    # on annule donc jusqu'à ce que le thread se soit debloqué, pour pouvoir join
                    self.__serial_port__.cancel_read()
            self.__reading_thread__.join()
            self.__reading_thread__ = None

    def enable_stub(self):
        """
        Enable the stub mode.
        """
        if not self.__stub__:
            self.__stub__ = True
            restore = self.__reading_thread__ is not None
            self.disable_async_read()
            self.clear()
            if restore:
                self.enable_async_read()

    def disable_stub(self):
        """
        Disable the stub mode.
        """
        if self.__stub__:
            self.__stub__ = False
            restore = self.__reading_thread__ is not None
            self.disable_async_read()
            self.clear()
            if restore:
                self.enable_async_read()

    def add_stubbed_answer(self, bytestream):
        """
        This will add a stubbed answer to the input immediately
        """
        if self.__stub__:
            if type(bytestream) is str:
                if len(bytestream) % 2 != 0:
                    bytestream = f"0{bytestream}"
                bytestream = bytearray.fromhex(bytestream)
            with self.__lock__:
                self.__input_bytestream__ += bytestream

    def add_query_stubbed_answer(self, bytestream):
        """
        This will add a stubbed answer to the input immediately if the stream is empty,
        else it will stack the answers and only add them one by one after the stream have been read
        """
        if self.__stub__:
            if type(bytestream) is str:
                if len(bytestream) % 2 != 0:
                    bytestream = f"0{bytestream}"
                bytestream = bytearray.fromhex(bytestream)
            with self.__lock__:
                self.__query_stub_input__.append(bytestream)

    def clear(self):
        with self.__lock__:
            if self.log_enabled:
                self.__log_callback_rx__(self.current_log_file, None, self.__input_bytestream__)
            self.__input_bytestream__.clear()
            self.protocol.resync()

    def __read_routine__(self):
        """
        routine used by the reading thread when enable_async_read is active.
        This routine will continuously process bytes from the serial ports and fill the self._frames_queue with parsed frames.
        If self.__received_frame_callback__ is set, the routine will continuously flush the queue, calling the callback for each frame.
        If self.__received_frame_callback__ is not set, user shall call the read method to flush the queue manually.
        """
        tmp = bytearray()
        while self.__async_enabled__.is_set():
            try:
                tmp.clear()
                if self.is_opened:
                    if not self.__stub__:
                        tmp += self.__serial_port__.read(1)  # bloquant
                        tmp += self.__serial_port__.read(self.__serial_port__.in_waiting)
                    # If we are disabling async read, exit immediately or deadlock may occur
                    if not self.__async_enabled__.is_set():
                        break
                    # Only lock when blocking read on __serial_port__ is over
                    with self.__lock__:
                        self.__input_bytestream__ += tmp
                        self.__protocol__.process_bytestream(self.__input_bytestream__)
                        if self.__received_frame_callback__ is not None:
                            self.read()
                else:
                    time.sleep(0.5)
            except Exception as e:
                print("pyserial_device.__read_toutine__:", e)

    @property
    def current_log_file(self):
        return self.__log_file__
