import datetime
from queue import Queue


class packet_raw(object):
    """Class representing a binary packet.

    Any packet is composed of HEADER, DATA, FOOTER, no matter protocol.
    """

    def __init__(self):
        self._header = bytearray()
        self._data = bytearray()
        self._footer = bytearray()
        self._data_length = 0

    @property
    def header(self):
        return self._header

    @property
    def data(self):
        return self._data

    @property
    def footer(self):
        return self._footer

    def build_from_raw_bytes(self, in_frame):
        """Create a packet (header, data, and footer) from received frame.

        Args:
            in_frame: bytearray or hexa string of the received frame.
        """
        if type(in_frame) is str:
            in_frame = bytearray.fromhex(in_frame)
        self._data = in_frame
        self._data_length = len(self._data)

    def build_from_protocol_infos(self, protocol_infos={}, data_field=""):
        """Used create a packet (header, data, and footer) from serialization informations, and a data field.

        Args:
            protocol_info (dic) : contains the protocol information
            data_field (str) : data of the packet

        """
        data = data_field
        if type(data) is str:
            data = bytearray.fromhex(data)
        self._data = data
        self._data_length = len(self._data)

    def is_valid(self):
        """Check if the packet is valid according to protocol.

        returns:
            (status, message)
        """
        return True, "OK"

    @property
    def as_bytes(self):
        return self._header + self._data + self._footer

    @property
    def as_hexa_str(self):
        return self._header.hex().upper() + self._data.hex().upper() + self._footer.hex().upper()

    def __str__(self):
        """Returns a string representation of the packet.

        Header, data and footer separated by '|'.
        Automatically called using print, format, or conversion to str object.
        """
        packet = self._header.hex() + " | " + self._data.hex() + " | " + self._footer.hex()
        return packet.upper()

    def __getitem__(self, index):
        """
        Allow to get byte by index.
        """
        packet = self.as_bytes
        if index < len(packet):
            return packet[index]
        return None


class protocol_raw(object):
    """Basic protocol that takes the bytestream as is.

    If used to create a higher level protocol, modify if necessary:
    HEADER_SIZE
    __search_start_of_frame__ : define start_index and if possible __nb_missing_bytes__.
    __search_end_of_frame__ : to be defined if __nb_missing_bytes__ was not definied in __search_start_of_frame
    """

    HEADER_SIZE = 0

    def __init__(self):
        self.__frames_queue__ = Queue()
        self.__state__ = "searching_header"
        self.__start_index__ = 0
        self.__end_index__ = 0
        self.__nb_missing_bytes__ = 0

    # log feature
    @property
    def current_timestamp(self):
        return datetime.datetime.now().strftime("%Hh%Mm%Ss")

    def get_frames(self):
        answers = []
        while self.__frames_queue__.qsize() > 0:
            answers.append(self.__frames_queue__.get())
        return answers

    def build_packet_instance(self):
        return packet_raw()

    def __search_start_of_frame__(self, bytestream):
        """This function will be called by the default process_bytestream method.

        This function must:
            - set the start_index to the start of frame
            - if possible, set the nb_missing_bytes to be read after the header (depends on the HEADER_SIZE member)
            - return True if the start of frame was found,
            - return False otherwise

        Args:
            bytestream (bytearray): the bytearray to search the start_of_frame in

        Returns:
            bool: if the start_of_frame was found
        """
        self.__start_index__ = 0
        return True

    def __search_end_of_frame__(self, bytestream):
        """This function will be called by the default process_bytestream method.

        This function must:
            - set the end_index to the end of the frame (start_index is already set)
            - return True if the end of frame was found,
            - return False otherwise

        Args:
            bytestream (bytearray): the bytearray to search the start_of_frame in

        Returns:
            bool: if the end_of_frame was found
        """
        if len(bytestream[self.__start_index__ :]) >= self.HEADER_SIZE:  # check if header is complete
            self.__nb_missing_bytes__ = self.__compute_nb_missing_bytes_from_header__(bytestream)
            if len(bytestream[self.__start_index__ + self.HEADER_SIZE :]) >= self.__nb_missing_bytes__:
                self.__end_index__ = self.__start_index__ + (self.HEADER_SIZE - 1) + self.__nb_missing_bytes__
                return True
            else:
                return False
        else:
            return False

    def __compute_nb_missing_bytes_from_header__(self, bytestream):
        return len(bytestream[self.__start_index__ :])

    def __save_frame__(self, status, bytestream):
        """Private function.

        Used by process_bytestream to log processed frames (logged as received frames)
        """
        self.__frames_queue__.put((status, bytestream[:]))

    def process_bytestream(self, bytestream):
        """This functions takes a bytearray as input, an will process it to get frames.

        The processing is based on the __search_start_of_frame__, and __search_end_of_frame__ methods,
        that can be redefined in inherited classes to match the procotol.
        This function can also be redefined in inherited classes, if one want to bypass the automatic parsing bellow.
        When  frames are detected, they will be stored in the queue as tuple (True, frame), and removed from the bytestream .
        CAUTION: this function will eventually delete element from the input bytstream. NOT THREAD-SAFE (use with a lock if necessary).
        parameters:

        Args:
            bytestream (bytearray): the bytes to process.
        """
        processing = True
        while bool(bytestream) and processing:
            if self.__state__ == "searching_header":
                header_found = self.__search_start_of_frame__(bytestream)
                if header_found:
                    self.__state__ = "searching_end_of_frame"
                    if self.__start_index__ != 0:
                        self.__save_frame__(False, bytestream[0 : self.__start_index__])
                        del bytestream[0 : self.__start_index__]
                        self.__start_index__ = 0
                else:
                    self.__save_frame__(False, bytestream[:])
                    del bytestream[:]

            if self.__state__ == "searching_end_of_frame":
                end_of_frame_found = self.__search_end_of_frame__(bytestream)
                if end_of_frame_found:
                    self.__state__ = "searching_header"
                    packet = self.build_packet_instance()
                    candidate_raw_frame = bytestream[self.__start_index__ : self.__end_index__ + 1]
                    packet.build_from_raw_bytes(candidate_raw_frame)
                    if packet.is_valid()[0] is True:
                        self.__save_frame__(True, candidate_raw_frame[:])
                        del bytestream[self.__start_index__ : self.__end_index__ + 1]
                    else:
                        if self.__search_start_of_frame__(bytestream[self.__start_index__ + 1 :]):
                            self.__save_frame__(False, bytestream[0 : self.__start_index__ + 1])
                            del bytestream[0 : self.__start_index__ + 1]
                        else:
                            self.__save_frame__(False, bytestream[:])
                            del bytestream[:]
                    self.resync()
                else:
                    processing = False

    def resync(self):
        """Resync start index when looking for a frame in bytestream.

        To be redefined if necessary in inherited class, to reset the processing state
        """
        self.__state__ = "searching_header"
        self.__start_index__ = 0
        self.__end_index__ = 0

    def on_timeout(self):
        """This can be redefined to set a flag indicating that the time to wait for an answer is over.

        Said flag may be used by process_bytestream() to discard some erroneous data.
        """

    def default_log_tx(self, file_handler, status, frame):
        """Callback called to log transmitted frames. to be overload in inherited class to match specific needs.

        Args:
            status (bool)
            frame (bytearray): bytearray
        """
        try:
            if status:
                file_handler.write(f"{self.current_timestamp} [sent]: {frame.hex().upper()}\n")
            else:
                file_handler.write(f"{self.current_timestamp} [error]: {frame.hex().upper()}\n")
            file_handler.flush()
        except Exception as e:
            file_handler.write(f"{self.current_timestamp} [error]: {e}\n")
            file_handler.flush()
            print(f"error in default_log_callback_tx from {type(self).__name__}", e)

    def default_log_rx(self, file_handler, status, frame):
        """Callback called to log received frames. to be overload in inherited class to match specific needs.

        Args:
            status (bool):
            frame (bytearray): bytearray
        """
        try:
            if status is None:
                file_handler.write(f"{self.current_timestamp} [cleared]: {frame.hex().upper()}\n")
            elif status is True:
                file_handler.write(f"{self.current_timestamp} [recv]: {frame.hex().upper()}\n")
            else:
                file_handler.write(f"{self.current_timestamp} [rejected]: {frame.hex().upper()}\n")
            file_handler.flush()
        except Exception as e:
            file_handler.write(f"{self.current_timestamp} [error]: {e}\n")
            file_handler.flush()
            print(f"error in default_log_callback_rx from {type(self).__name__}", e)
