from setuptools import setup

setup(
    name="pyserial_device",
    version="0.1.0",
    description="A layer built on PySerial, to provide more advanced uses",
    url="https://gitlab.com/Golden21/pyserial_device",
    author="Audry Kouanda",
    author_email="audrykouanda21@gmail.fr",
    license="free",
    install_requires=[
        "pyserial>=3.0",
    ],
    classifiers=[
        "Development Status :: 1 - work in progress",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: BSD License",
        "Operating System :: Windows",
        "Programming Language :: Python :: 3",
    ],
    packages=["pyserial_device"],
)
