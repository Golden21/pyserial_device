# pyserial_device

an advanced use of the pyserial library, made to easily provide features for serial communication.
This provides the following features:
- continuous reading capability
- communication logging
- callback on received messages
- efficient read, write, and query
- automated parsing of received bytes into frames, according to any user-defined protocol
- testability using stubbed mode